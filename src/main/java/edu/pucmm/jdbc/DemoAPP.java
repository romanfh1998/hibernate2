package edu.pucmm.jdbc;

import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.routes.IndexRoute;
import edu.pucmm.jdbc.routes.students.DeleteStudent;
import edu.pucmm.jdbc.routes.students.EditStudent;
import edu.pucmm.jdbc.routes.students.NewStudent;
import java.util.HashMap;
import java.util.Map;
import spark.ModelAndView;
import spark.Spark;
import static spark.Spark.get;

/**
 * @author a.marte
 */
public class DemoAPP {
    private static String USER = "admin";
    private static String PASS = "1234";

       public static void main(String[] args) {
        Spark.port(8080);
        addGetRoute(new IndexRoute());
        addGetRoute(new NewStudent());
        addGetRoute(new EditStudent());
        addGetRoute(new DeleteStudent());
        Spark.init();
    
 get("/logout" ,(req,res)
                 -> {
            res.removeCookie("admin");
           return "1";
 
        });
 get("/registrar", (request, response)
                -> {
            String user = request.queryParams("user");
            String pass = request.queryParams("pass");
            System.out.println(user);
             System.out.println(pass);
            if (user.equals(USER) && pass.equals(PASS)) {
                // salvo el cookie de session
                return "1";
            } else {
                return "0";
            }
    
             });

        get("/check/login", (request, response)
                -> {
            String user = request.queryParams("user");
            String pass = request.queryParams("pass");
            System.out.println(user);
             System.out.println(pass);
            if (user.equals(USER) && pass.equals(PASS)) {
                // salvo el cookie de session
                return "1";
            } else {
                return "0";
            }
    
             });

    }
 
    private static void addGetRoute(RouteBase routeBase) {
        Spark.get(routeBase.path, routeBase);
    }

}
