package edu.pucmm.jdbc.routes.students;

import edu.pucmm.jdbc.domains.User;
import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.utils.HibernateUtil;


import org.hibernate.Session;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.SessionFactory;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class registrar extends RouteBase {

    public registrar() {
        super("/registrar");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        //id
        
        
        String idstr = request.queryParams("id");
        String names = request.queryParams("names");
        String lastnames = request.queryParams("lastnames");
        String birthStr = request.queryParams("birdth");
       
        
        
        System.out.println("ensenar los valores que esta en java id=" + idstr +" names = "+ names + " " + lastnames + " " + birthStr);
        
        if (names != null && lastnames != null && birthStr!= null
                && !names.isEmpty() && !lastnames.isEmpty() && !birthStr.isEmpty()) {
            // TODO el insert puede estar mal
            try{
                
                        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
                    Date parsed = format.parse(birthStr);
                    java.sql.Date birth = new java.sql.Date(parsed.getTime());
         int id = Integer.parseInt(idstr);
                    //----------------------------------------------------
                    
                      User user = new User();
                      user.setId(id);
                    user.setNames(names);
                         user.setLastnames(lastnames);
                           
                         user.setBirthDate(birth);
                         
                         
                          
                    //Create session factory object
                    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                    //getting session object from session factory
                    Session session = sessionFactory.openSession();
                    //getting transaction object from session object
                    session.beginTransaction();

                    session.save(user);
                    
                    session.getTransaction().commit();
                    
                    System.out.println("Inserted Successfully");
                    //session.close();
                    //sessionFactory.close();
                    
                    //-----------------------------------------------------
          
           
            }    catch (Exception e) {
                
                 e.printStackTrace();
            } 
            return "1";
        }
        return "0";
    }
}
