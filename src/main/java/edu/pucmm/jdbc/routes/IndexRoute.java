package edu.pucmm.jdbc.routes;

import edu.pucmm.jdbc.domains.User;
import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.utils.HibernateUtil;

import org.hibernate.Query;

import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class IndexRoute extends RouteBase {

    public IndexRoute() {
        super("/");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("html/index.html");
        VelocityContext context = new VelocityContext();
        List<User>user = new ArrayList<>();
        
        //       //Create session factory object
          SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
          //getting session object from session factory
          Session session = sessionFactory.openSession();
          //getting transaction object from session object
          session.beginTransaction();
          Query query = session.createQuery("from Student");
          
          ArrayList<User> UserList =  (ArrayList<User>) query.list();
          for(User User : UserList)
        {
          System.out.println("id: "+User.getId()+", Name: "+User.getNames()+", lastnames: "+User.getLastnames()+"birthdate: "+User.getBirthDate());
        }
        session.getTransaction().commit();
        //sessionFactory.close();
 
              
              
        context.put("Userlist", UserList);
        
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

}
