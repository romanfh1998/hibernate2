package edu.pucmm.jdbc.utils;

import javax.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static SessionFactory sessionFactory ;
	
	static {
            
                try
                {
                 sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
                }
                catch(Throwable th){
                 System.err.println("Enitial SessionFactory creation failed"+th);
                 throw new ExceptionInInitializerError(th);
                }
              }
//    	Configuration configuration = new Configuration().configure();
//    	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
//        sessionFactory = configuration.buildSessionFactory(builder.build());
//     }
        @Transactional
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
